---
layout: markdown_page
title: "Redmine"
---
<!-- This is the template for defining sections which will be included in a tool comparison page. This .md file is included in the top of the page and a table of feature comparisons is added directly below it. This template provides the sections which can be included, and the order to include them. If a section has no content yet then leave it out. Leave this note in tact so that others can see where new sections should be added.

## Summary
   - minimal requirement <-- comment. delete this line
## Strengths
## Challenges
## Who buys and why
## Comments/Anecdotes
   - possible customer issues with product  <-- comment. delete this line
   - sample benefits and success stories  <-- comment. delete this line
   - date, source, insight  <-- comment. delete this line
## Resources
   - links to communities, etc  <-- comment. delete this line
   - bulleted list  <-- comment. delete this line
## FAQs
 - about the product  <-- comment. delete this line
## Integrations
## Pricing
   - summary, links to tool website  <-- comment. delete this line
### Value/ROI
   - link to ROI calc?  <-- comment. delete this line
## Questions to ask
   - positioning questions, traps, etc.  <-- comment. delete this line
## Comparison
   - features comparison table will follow this <-- comment. delete this line

<!------------------Begin page additions below this line ------------------ -->

## On this page
{:.no_toc}

- TOC
{:toc}

## Summary
Redmine is a free and open source, web-based project management and issue tracking tool. It allows users to manage multiple projects and associated sub-projects. It features per project wikis and forums, time tracking, and flexible, role-based access control. It includes a calendar and Gantt charts to aid visual representation of projects and their deadlines. Redmine integrates with various version control systems and includes a repository browser and diff viewer.

Redmine is written using the Ruby on Rails framework. It is cross-platform and cross-database and supports 34 languages.

## Comments/Anecdotes
* From [a user on Quora](https://www.quora.com/What-is-your-experience-using-Redmine-for-project-management-and-collaborating)
  > it's flexible, highly customizable, (you can choose between various templates, plugins, etc.) It's open source and free to use. [and] the user interface doesn't overwhelm you.
  >
  > Since it's highly customizable you really have to dig yourself into it at first in order to create a great experience for your team members and colleagues / clients

## Resources
* [Redmine homepage](https://www.redmine.org/projects/redmine/wiki)
* [Redmine Wikipedia](https://en.wikipedia.org/wiki/Redmine)
* [G2 Crowd review page](https://www.g2crowd.com/products/redmine/reviews)

## Pricing
* $0 - Redmine is free and open source
* However, TCO can be high, as Redmine is similar to Jenkins, with lots of desired functionality available through plugins (944 as of 2018-10-17), some of which are maintained, and some of which are not.
* There are also companies which sell plug-in sets, for example to add Agile functionality (eg. [RedmineUP Agile Plugin](https://www.redmineup.com/pages/plugins/agile)). Prices vary.

## Comparison
